# API client TDD

Simple PHP API client application. Receives API data and displays the content.
Content can be filtered by publications and popular tags.

Publications are placed at the top of the page and tags at the bottom of the page.

Go to project root and install project requirements: 

```shell
composer install
```

Routes are set in: `public/index.php`

## PHPUnit tests
- Location: `tests/unit/`

To run PHPUnit tests use this command:

```sh
php vendor/bin/phpunit --bootstrap vendor/autoload.php tests/unit/*
```

## Acceptance tests
- Location: `tests/acceptance/`

Before you can run Acceptance tests you must install:

- Selenium server
- PhantomJS server
- Chromium web driver

To run Acceptance tests use this command:

```sh
php vendor/bin/codecept run --env dev_chrome --debug
```