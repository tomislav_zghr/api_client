<?php

namespace App\Models;
use Core\Model;
use Respect\Validation\Validator as v;


class Article extends Model
{

    private $validator;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $excerpt;

    /**
     * @var array
     */
    private $images;

    public function __construct()
    {
        $this->validator = v::attribute('title', v::stringType(), '' )
            ->attribute('excerpt', v::stringType())
            ->attribute('images', v::arrayType())
        ;
    }

    /**
     * @return v
     */
    public function getValidator(): v
    {
        return $this->validator;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getExcerpt(): string
    {
        return $this->excerpt;
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt)
    {
        $this->excerpt = $excerpt;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages(array $images)
    {
        $this->images = $images;
    }



}
