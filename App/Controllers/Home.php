<?php

namespace App\Controllers;


use Core\View;
use Core\Controller;
use GuzzleHttp;
use App\Models\Article;
use Respect\Validation\Validator;


class Home extends Controller
{

    protected function before()
    {
        //return false;
    }

    protected function after()
    {
        //echo " (after)";
    }

    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function indexAction()
    {
        $errors = [];
        $client = new GuzzleHttp\Client();
        if(isset($this->route_params['slug'])) {
            $urlData = 'tag/' . $this->route_params['slug'];
            $title = $this->route_params['slug'];
        } else if(isset($this->route_params['publication'])) {
            $urlData = '' . $this->route_params['publication'];
            $title = $this->route_params['publication'];
        } else {
            $urlData = '';
            $title = '';
        }
        $url = 'http://api.thejournal.ie/v3/sample/';

        try {
            $res = $client->request('GET', $url . $urlData, [
                'auth' => ['sample', 'eferw5wr335£65']
            ]);
            $json = $res->getBody()->getContents();
        } catch (\Exception $exception){
            $json = '';
            $errors[] = "Please wait for 5 seconds and reload the page";
        }

        $jsonDecoded = (json_decode($json, true));

        $articles = [];
        if(isset($jsonDecoded['response']['articles'])) {
            foreach($jsonDecoded['response']['articles'] as $articleData) {
                if(isset($articleData['type']) && $articleData['type'] == 'post') {
                    $article = new Article();
                    $article->setTitle($articleData["title"]);
                    $article->setExcerpt($articleData["excerpt"]);
                    $article->setImages($articleData["images"]);

                    /**
                     * @var Validator $validator
                     */
                    $validator = $article->getValidator();

                    try{
                        $validator->check($article);

                        $articles[] = $article;

                    } catch (\Exception $error){
                        $errors[] = $error->getMessage();
                    }
                }
            }
        }
        $publications = [
            'thejournal',
            'thescore',
            'thedailyedge',
            'businessetc'
        ];

        $slugs = [
            'google',
            'apple',
            'hogan-knows-best'
        ];


        View::renderTemplate('Home/index.html', [
            'publications' => $publications,
            'slugs' => $slugs,
            'title' => $title,
            'articles' => $articles,
            'errors' => $errors
        ]);
    }
}
