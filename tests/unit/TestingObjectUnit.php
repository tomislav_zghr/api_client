<?php

namespace unit;

use PHPUnit\Framework\TestCase;
use App\Models\Article;


class TestingObjectUnit extends TestCase
{
    public function testPushAndPop()
    {
        $article = new Article;

        $article->setExcerpt("Test excerpt");
        $article->setTitle("Test title");
        $article->setImages([
           [
               'thumbnail' => ['image' => 'image.jpg']
           ]
        ]);

        $this->assertEquals("Test excerpt", $article->getExcerpt());
        $this->assertEquals("Test title", $article->getTitle());
        $this->assertContains('image.jpg', $article->getImages()[0]['thumbnail']);
    }
}



