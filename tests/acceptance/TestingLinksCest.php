<?php


class TestingLinksCest
{


    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/');
    }

    public function _after(AcceptanceTester $I)
    {
    }


    /**
     * @param AcceptanceTester $I
     * @param \Testing\TestingPublications $testingPublications
     * @param \Testing\TestingTags $testingTags
     * @throws Exception
     */
    public function tryToTest(AcceptanceTester $I, \Testing\TestingPublications $testingPublications, \Testing\TestingTags $testingTags)
    {

        $testingPublications->clickTestingPublications();
        $testingTags->clickTestingTags();

    }
}
