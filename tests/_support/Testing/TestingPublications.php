<?php
namespace Testing;


class TestingPublications
{

    /**
     * @var \AcceptanceTester
     */
    protected $tester;

    public  $homeLink = '#publication-';
    public  $theJournalLink = '#publication-thejournal';
    public  $theScoreLink = '#publication-thescore';
    public  $theDailyedgeLink = '#publication-thedailyedge';
    public  $businesseticLink = '#publication-businessetc';

    public function __construct(\AcceptanceTester $I)
    {
        $this->tester = $I;

    }

    /**
     * @throws \Exception
     */
    public function clickTestingPublications()
    {
        $I = $this->tester;
        $I->wantTo('Test journal link click');
        $I->click($this->theJournalLink);
        $I->waitForText("Thejournal", 20, "#title");
        $I->click($this->theScoreLink);
        $I->waitForText("Thescore", 20, "#title");
        $I->click($this->theDailyedgeLink);
        $I->waitForText("Thedailyedge", 20, "#title");
        $I->click($this->businesseticLink);
        $I->waitForText("Businessetc", 20, "#title");
    }
}