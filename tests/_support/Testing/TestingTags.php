<?php
namespace Testing;


class TestingTags
{

    /**
     * @var \AcceptanceTester
     */
    protected $tester;

    public  $googleLink = '#tag-google';
    public  $appleLink = '#tag-apple';
    public  $thehoganknowsbestLink = '#tag-hogan-knows-best';

    public function __construct(\AcceptanceTester $I)
    {
        $this->tester = $I;

    }

    /**
     * @throws \Exception
     */
    public function clickTestingTags()
    {
        $I = $this->tester;
        $I->wantTo('Test tags link click');
        $I->click($this->googleLink);
        $I->waitForText("Google", 20, "#title");
        $I->click($this->appleLink);
        $I->waitForText("Apple", 20, "#title");
        $I->click($this->thehoganknowsbestLink);
        $I->waitForText("Hogan-knows-best", 20, "#title");
    }
}