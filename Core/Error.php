<?php

namespace Core;


class Error
{

    /**
     * @param $level
     * @param $message
     * @param $file
     * @param $line
     * @throws \ErrorException
     */
    public static function errorHandler($level, $message, $file, $line)
    {
        if (error_reporting() !== 0) {  // to keep the @ operator working
            throw new \ErrorException($message, 0, $level, $file, $line);
        }
    }

    /**
     * @param $exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function exceptionHandler($exception)
    {
        // Code is 404 (not found) or 500 (general error)
        $code = $exception->getCode();
        if ($code != 404) {
            $code = 500;
        }
        http_response_code($code);

        if (\App\Config::SHOW_ERRORS) {
            $message = "<h1>Fatal error</h1>";
            $message .=  "<p>Uncaught exception: '" . get_class($exception) . "'</p>";
            $message .=   "<p>Message: '" . $exception->getMessage() . "'</p>";
            $message .=   "<p>Stack trace:<pre>" . $exception->getTraceAsString() . "</pre></p>";
            $message .=   "<p>Thrown in '" . $exception->getFile() . "' on line " . $exception->getLine() . "</p>";
            dump($message);
        } else {
            $log = dirname(__DIR__) . '/logs/' . date('Y-m-d') . '.txt';
            ini_set('error_log', $log);

            $message = "Uncaught exception: '" . get_class($exception) . "'";
            $message .= " with message '" . $exception->getMessage() . "'";
            $message .= "\nStack trace: " . $exception->getTraceAsString();
            $message .= "\nThrown in '" . $exception->getFile() . "' on line " . $exception->getLine();

            error_log($message);

            View::renderTemplate($code . ".html");
        }
    }
}
